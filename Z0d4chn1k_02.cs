﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z0d4ch1
{
    public partial class Z0d4chn1k_02
    {
        public static void Beg_4()
        {
            double d = double.Parse(Console.ReadLine());
            Console.WriteLine(d * 3.14);
        }
        public static void Beg_21()
        {
            double[,] points = new double[3, 2]; string[] tmp;
            for (int i = 0; i < 3; i++)
            {
                tmp = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                points[i, 0] = double.Parse(tmp[0]);
                points[i, 1] = double.Parse(tmp[1]);
            }
            double Distance(double x1, double x2, double y1, double y2)
                => Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            double[] distances = new double[]
            {
                Distance(points[0, 0], points[1, 0], points[0, 1], points[1,1]),
                Distance(points[1, 0], points[2, 0], points[1, 1], points[2,1]),
                Distance(points[0, 0], points[2, 0], points[0, 1], points[2,1])
            };
            double pp = (distances[0] + distances[1] + distances[2]) / 2;
            Console.WriteLine(Math.Sqrt(pp * (pp - distances[0]) * (pp - distances[1]) * (pp - distances[2])));
        }
        public static void Integer_4()
        {
            int a = 10, b = 2;
            Console.WriteLine($"In A - {a / b} line segments ");
        }
        public static void Boolean_4()
        {
            int a = 4, b = 3;
            Console.WriteLine(a > 2 && b <= 3 ? "yes" : "no");
        }
        public static void Boolean_21()
        {
            int num = 523;
            Console.WriteLine(num / 100 < num / 10 - num / 100 * 10 && num / 10 - num / 100 * 10 < num % 10 ? "yes" : "no");
        }
        public static void If_4()
        {
            int cnt = 0, n1 = 2, n2 = -2, n3 = 4;
            if (n1 > 0) cnt++;
            if (n2 > 0) cnt++;
            if (n3 > 0) cnt++;
            Console.WriteLine(cnt);
        }
        public static void Case_4()
        {
            int m = 2;
            switch (m)
            {
                case 3: goto case 1;
                case 5: goto case 1;
                case 7: goto case 1;
                case 8: goto case 1;
                case 10: goto case 1;
                case 12: goto case 1;
                case 1: Console.WriteLine(31); break;
                case 2: Console.WriteLine(28); break;
                case 6: goto case 4;
                case 9: goto case 4;
                case 11: goto case 4;
                case 4: Console.WriteLine(30); break;
            }
        }
        public static void For_4()
        {
            double price = 34.25;
            for (int i = 1; i < 11; i++)
                Console.WriteLine($"{i} - {i * price}");
        }
        public static void For_21()
        {
            int n = int.Parse(Console.ReadLine());
            double sum = 1;
            int Factorial(int n) => n == 1 ? 1 : n * Factorial(n - 1);
            for (int i = 1; i <= n; i++)
                sum += 1 / (Factorial(i));
            Console.WriteLine(sum);
        }
        public static void For_38()
        {
            int n = int.Parse(Console.ReadLine());
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += Math.Pow(i, n);
                n--;
            }
            Console.WriteLine(sum);
        }
        public static void While_4()
        {
            int n = int.Parse(Console.ReadLine());
            while (n > 0) n -= 3;
            if (n == 0) Console.WriteLine("True");
            else Console.WriteLine("False");
        }
        public static void Series_4()
        {
            int n = int.Parse(Console.ReadLine());
            double tmp = 0, sum = 0, mul = 1;
            for (int i = 0; i < n; i++)
            {
                tmp = double.Parse(Console.ReadLine());
                sum += tmp;
                mul *= tmp;
            }
            Console.WriteLine($"sum:{sum}\nmultiply:{mul}");
        }
        public static void Series_21()
        {
            int n = int.Parse(Console.ReadLine());
            double tmp = 0, prev = 0; bool condition = true;
            for (int i = 0; i < n; i++)
            {
                tmp = double.Parse(Console.ReadLine());
                if (tmp < prev)
                    condition = false;
                prev = tmp;
            }
            Console.WriteLine(condition);
        }
        public static void Proc_4()
        {
            void TrianglePS(double a, out double p, out double s)
            {
                s = a * a;
                p = 3 * a;
            }
            double tmpp = 0, tmps = 0;
            Random rnd = new Random();
            for (int i = 0; i < 3; i++)
            {
                TrianglePS(rnd.NextDouble(), out tmpp, out tmps);
                Console.WriteLine($"{i + 1}.\nPerimetr:{tmpp}\nSquare:{tmps}");
            }
        }
        public static void Proc19()
        {
            double RingS(double r1, double r2) => r1 < r2 ? double.NaN : 3.14 * Math.Pow(r1, 2) - 3.14 * Math.Pow(r2, 2);
            Console.WriteLine($"R1: {2.1}\tR2: {1.4} - result: {RingS(2.1, 1.4)}");
            Console.WriteLine($"R1: {5.3}\tR2: {2} - result: {RingS(5.3, 2)}");
            Console.WriteLine($"R1: {1.2}\tR2: {10.2} - result: {RingS(1.2, 10.2)}");
        }
        public static void Minmax4()
        {
            int n = 5; Random rnd = new Random();
            int[] nums = new int[n];
            for (int i = 0; i < n; i++)
                nums[i] = rnd.Next();
            Console.WriteLine(nums.OrderBy(v => v).FirstOrDefault());
        }
        public static void Array4()
        {
            int n = 5, a = 25, d = 5;
            double[] arr = new double[n];
            arr[0] = a;
            for (int i = 0; i < n; i++)
            {
                arr[i] = a + (i == 0 ? 0 : Math.Pow(d, i));
                Console.WriteLine(arr[i]);
            }
        }
        public static void Array21()
        {
            int n = 5, k = 2, l = 5; double sum = 0;
            int[] nums = new int[n]; Random rnd = new Random();
            for (int i = 0; i < n; i++)
                nums[i] = rnd.Next();
            for (int i = k; i < l; i++)
                sum += nums[i];
            Console.WriteLine(sum / l - k);
        }
        public static void Array54()
        {
            int n = 5; int[] a = new int[n];
            List<int> b = new List<int>(); Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next();
                if (a[i] % 2 == 0)
                    b.Add(a[i]);
            }
            Console.WriteLine($"capacity:{b.Count}");
            foreach (var item in b) Console.WriteLine(item);
        }
        public static void Array68()
        {
            int n = 5, mxind = 0, mnind = 0; int[] a = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next();
                Console.WriteLine(a[i]);
            }
            for (int i = 0; i < n; i++)
            {
                mxind = a[i] > a[mxind] ? i : mxind;
                mnind = a[i] < a[mnind] ? i : mnind;
            }
            Console.WriteLine();
            int tmp = a[mxind];
            a[mxind] = a[mnind];
            a[mnind] = tmp;
            foreach (var item in a)
                Console.WriteLine(item);
        }
        public static void Array93()
        {
            int n = 10; int[] a = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next();
                Console.WriteLine(a[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < n; i = i + 2)
                Console.WriteLine(a[i]);
        }
        public static void Array115()
        {
            int n = 10; int[] a = new int[n], l = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next(15);
                l[i] = i;
                Console.WriteLine($"{l[i]}){a[i]}");
            }
            for (int i = 0; i < n - 1; i++)
                for (int j = 0; j < n - i - 1; j++)
                    if (a[l[j]] > a[l[j] + 1])
                    {
                        int tmp = l[j];
                        l[j] = l[j + 1];
                        l[j + 1] = tmp;
                    }
            Console.WriteLine();
            a = a.OrderBy(v => v).ToArray();
            foreach (var item in l)
                Console.WriteLine($"{item}){a[item]}");
        }
        public static void Array119()
        {
            int n = 5; int[] a = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = int.Parse(Console.ReadLine());
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                if (i + 1 < n)
                {
                    if (a[i + 1] != a[i])
                    {
                        for (int j = n - 1; j > i; j--) a[j] = a[j - 1];
                        i++;
                    }
                }
            }
            a[n - 1] = a[n - 2];
            for (int i = 0; i < n; i++)
                Console.WriteLine(a[i]);
        }
        public static void Array134()
        {
            int n = 8; double xa = 0, xb = 0, ya = 0, yb = 0; double[] a = new double[n];
            double maxDist = double.MinValue;
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
                a[i] = rnd.Next(15);
            for (int i = 2; i < n; i += 2)
            {
                if (Math.Sqrt(Math.Pow(a[i] - a[i - 2], 2) + Math.Pow(a[i + 1] - a[i - 1], 2)) > maxDist)
                {
                    maxDist = Math.Sqrt(Math.Pow(a[i] - a[i - 2], 2) + Math.Pow(a[i + 1] - a[i - 1], 2));
                    xb = a[i]; xa = a[i - 2]; yb = a[i + 1]; ya = a[i - 1];
                }
            }
            Console.WriteLine($"Max distance equal: {maxDist} between dots \nA:{xa};{ya}\nB:{xb};{yb}");
        }
        public static void Matrix4()
        {
            int m = 3, n = 2; int[] nums = new int[n];
            for (int i = 0; i < n; i++)
                nums[i] = int.Parse(Console.ReadLine());
            Console.WriteLine();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write($"{nums[j]} ");
                Console.WriteLine();
            }
        }
        public static void Matrix20()
        {
            int m = 5, n = 3, mul = 1; int[,] mat = new int[m, n];
            Random rnd = new Random();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    mat[i, j] = rnd.Next(15);
                    Console.Write(mat[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                mul = 1;
                for (int j = 0; j < m; j++)
                    mul *= mat[j, i];
                Console.WriteLine(mul);
            }
        }
        public static void Matrix50()
        {
            int m = 5, n = 3, mx, mn; int[,] mat = new int[m, n];
            Random rnd = new Random();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    mat[i, j] = rnd.Next(15);
                    Console.Write(mat[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                mx = 0; mn = 0;
                for (int j = 1; j < m; j++)
                {
                    if (mat[j, i] > mat[mx, i]) mx = j;
                    if (mat[j, i] < mat[mn, i]) mn = j;
                }
                mat[mn, i] = mat[mx, i] + mat[mn, i];
                mat[mx, i] = mat[mn, i] - mat[mx, i];
                mat[mn, i] = mat[mn, i] - mat[mx, i];
            }
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write($"{mat[i, j]} ");
                Console.WriteLine();
            }
        }
        public static void Matrix83()
        {
            int m = 5, sum = 0; int[,] mat = new int[m, m];
            Random rnd = new Random();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mat[i, j] = rnd.Next(9);
                    Console.Write(mat[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            for (int i = 0; i < m; i++)
                sum += mat[i, i];
            Console.WriteLine(@" \ - " + sum); sum = 0;
            for (int i = m - 1, j = 0; i > -1; i--, j++)
                sum += mat[i, j];
            Console.WriteLine(@" / - " + sum);
        }
        public static void String4()
        {
            int n = int.Parse(Console.ReadLine());
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (n > 1 && n < 27)
                for (int i = 0; i < n; i++)
                    Console.Write(alphabet[i]);
            else
                Console.WriteLine("There are not so many letters in the alphabet");
        }
        public static void String16()
        {
            string str = Console.ReadLine();
            Console.WriteLine(str.ToLower());
        }
        public static void String29()
        {
            char c = char.Parse(Console.ReadLine());
            string s = Console.ReadLine(), subS = Console.ReadLine();
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == c)
                {
                    s = s.Insert(i, subS);
                    i += subS.Length + 1;
                }
            }
            Console.WriteLine(s);
        }
        public static void String44()
        {
            string str = Console.ReadLine(); int cnt = 0;
            foreach (var item in str.Split(' ', StringSplitOptions.RemoveEmptyEntries))
            {
                int locCnt = 0;
                for (int i = 0; i < item.Length; i++)
                {
                    if (item[i] == 'А')
                        locCnt++;
                }
                if (locCnt >= 3)
                    cnt++;
            }
            Console.WriteLine(cnt);
        }
        public static void String61()
        {
            string path = Console.ReadLine();
            string[] parts = path.Split(@"\", StringSplitOptions.RemoveEmptyEntries);
            for (int i = parts.Length - 1; i > -1; i--)
            {
                if (!parts[i].Contains('.'))
                {
                    Console.WriteLine(parts[i]);
                    break;
                }
            }
        }
        public static void File4()
        {
            string[] names = new string[] { "kek.txt", "pek.txt", "lol.txt", "gg.txt" };
            string path = @"C:\Users\User\Desktop\filesTest"; int cnt = 0;
            for (int i = 0; i < names.Length; i++)
            {
                FileInfo fi = new FileInfo(path + @"\" + names[i]);
                if (fi.Exists)
                    cnt++;
            }
            Console.WriteLine(cnt);
        }
        public static void File28()
        {
            string path = @"C:\Users\User\Desktop\filesTest\nums.txt";
            string inputText = "", outputText = "";
            using (StreamReader sr = new StreamReader(path))
            {
                inputText = sr.ReadLine();
                List<double> nums = new List<double>();
                foreach (var item in inputText.Split(' ', StringSplitOptions.RemoveEmptyEntries))
                    nums.Add(double.Parse(item));
                outputText += nums[0] + " ";
                for (int i = 1; i < nums.Count - 1; i++)
                    outputText += (nums[i - 1] + nums[i] + nums[i + 1]) / 3 + " ";
                outputText += nums[nums.Count - 1];
            }
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                sw.WriteLine(outputText);
            }
        }
        public static void File45()
        {
            string[] names = new string[] { "nums.txt", "empty.txt", "mid.txt" };
            string path = @"C:\Users\User\Desktop\filesTest";
            string tmpMax = "", tmpMin = "";
            List<FileInfo> fis = new List<FileInfo>();
            foreach (var item in names)
                fis.Add(new FileInfo(path + @"\" + item));
            string mxPath = fis.OrderBy(fi => fi.Length).FirstOrDefault().FullName;
            string mnPath = fis.OrderByDescending(fi => fi.Length).FirstOrDefault().FullName;
            using (StreamReader sr1 = new StreamReader(mxPath), sr2 = new StreamReader(mnPath))
            {
                tmpMax = sr1.ReadToEnd();
                tmpMin = sr2.ReadToEnd();
            }
            using (StreamWriter sw1 = new StreamWriter(mxPath), sw2 = new StreamWriter(mnPath))
            {
                sw1.WriteLine(tmpMin);
                sw2.WriteLine(tmpMax);
            }
        }
        public static void File51()
        {
            string[] names = new string[] { "file1.txt", "file2.txt", "file3.txt", "file4.txt" };
            string path = @"C:\Users\User\Desktop\filesTest", text = "";
            for (int i = 0; i < names.Length - 1; i++)
                using (StreamReader sr = new StreamReader(path + @"\" + names[i]))
                {
                    text += sr.ReadToEnd();
                }
            text = new string(text.OrderByDescending(v => v).ToArray());
            using (StreamWriter sw = new StreamWriter(path + @"\" + names[names.Length - 1]))
            {
                sw.WriteLine(text);
            }
        }
        public static void File61()
        {
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            string aLS = "";
            using (StreamReader sr = new StreamReader(path))
            {
                string[] strings = sr.ReadToEnd().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                aLS = strings[strings.Length - 1];
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(aLS);
            }
        }
        public static void File71()
        {
            int i = int.Parse(Console.ReadLine()), j = int.Parse(Console.ReadLine());
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            List<List<double>> matrix = new List<List<double>>();
            using (StreamReader sr = new StreamReader(path))
            {
                int indx = 0; string[] strings;
                while (!sr.EndOfStream)
                {
                    matrix.Add(new List<double>());
                    foreach (var item in sr.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries))
                        matrix[indx].Add(double.Parse(item));
                    indx++;
                }
            }
            try { Console.WriteLine(matrix[i - 1][j - 1]); }
            catch (Exception ex) { Console.WriteLine(0); }
        }
        public static void Text4()
        {
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            int rows = 0, symbols = 0;
            using (StreamReader sr = new StreamReader(path))
            {
                string str = "";
                while (!sr.EndOfStream)
                {
                    str += sr.ReadLine();
                    rows++;
                }
                symbols = str.Length;
            }
            Console.WriteLine($"Rows:{rows}\nSymbols:{symbols}");
        }
        public static void Text27()
        {
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            int k = int.Parse(Console.ReadLine()); string tmp = "";
            using (StreamReader sr = new StreamReader(path))
            {
                bool isThatAbz = false;
                string str; int nAbz = 0;
                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine();
                    if (str.Contains("     "))
                        nAbz++;
                    if (k - 1 == nAbz)
                        isThatAbz = true;
                    if (!isThatAbz)
                        tmp += str;
                }
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(tmp);
            }
        }
        public static void Text56()
        {
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            string text = "";
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    foreach (var item in sr.ReadLine())
                        if (!text.Contains(item))
                            text += item;
                }
                text = new string(text.OrderByDescending(c => c).ToArray());
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(text);
            }
        }
        public static void Param4()
        {
            double[] Invert(double[] a, int n) => a.Reverse().ToArray();
            double[] A = new double[] { 5.6, 9.8, 4, 2, 1, 2, 3.6 };
            double[] B = new double[] { 3.7, 9.28, 47, 12, 17, 42, 63.6 };
            double[] C = new double[] { 8.9, 9.2, 6.4, 7.8, 8, 6, 1.52 };
            foreach (var item in Invert(A, A.Length))
                Console.Write(item + " ");
            Console.WriteLine();
            foreach (var item in Invert(B, B.Length))
                Console.Write(item + " ");
            Console.WriteLine();
            foreach (var item in Invert(C, C.Length))
                Console.Write(item + " ");
        }
        public static void Param33()
        {
            string LowCaseRus(string s) => s.ToLower();
            string s1 = "иавРАВЫРАмармврЫАМЫРАМ"; string s2 = "авЫАвываыаЫАВАЫ";
            string s3 = "аывА№;364АВЫавЫыва"; string s4 = "ВЫА45аыв4аы3455ываыВАЫФ";
            string s5 = "аВЫАЫАуы443ыапы%№45";
            Console.WriteLine($"{LowCaseRus(s1)}\n{LowCaseRus(s2)}\n{LowCaseRus(s3)}\n{LowCaseRus(s4)}\n{LowCaseRus(s5)}");
        }
        public static void Param51()
        {
            void AddLineNumbers(string s, int n, int k, int l)
            {
                string str = "";
                using (StreamReader sr = new StreamReader(s))
                {
                    while (!sr.EndOfStream)
                    {
                        str += $@"{sr.ReadLine()}";
                        str = str.Insert(k, l * ' ' + n.ToString() + l * ' ');
                        n++;
                    }
                }
                using (StreamWriter sw = new StreamWriter(s))
                {
                    sw.WriteLine(str);
                }
            }
            string path = @"C:\Users\User\Desktop\filesTest\file.txt";
            AddLineNumbers(path, 4, 5, 3);
        }
        public static void Param62()
        {
            TDate PrevDate(TDate d)
            {
                if (d.CheckDate() == 0)
                {
                    if (d.Day == 1)
                    {
                        d.Month--;
                        d.Day = d.DaysInMonth();
                    }
                    else
                        d.Day -= 1;
                }
                return d;
            }
            TDate d1 = new TDate(32, 4, 4322);
            TDate d2 = new TDate(15, 3, 432);
            TDate d3 = new TDate(1, 12, 8922);
            TDate d4 = new TDate(18, 1, 822);
            TDate d5 = new TDate(22, 6, 722);
            Console.WriteLine($"{PrevDate(d1)}\n{PrevDate(d2)}\n{PrevDate(d3)}\n{PrevDate(d4)}\n{PrevDate(d5)}");
        }
        public static void Recur4()
        {
            int Fib1(int n) => n < 3 ? 1 : Fib1(n - 2) + Fib1(n - 1);
            int a1 = Fib1(5), a2 = Fib1(10), a3 = Fib1(3), a4 = Fib1(6), a5 = Fib1(7);
            Console.WriteLine($"{a1}\n{a2}\n{a3}\n{a4}\n{a5}");
        }
        public static void Recur17()
        {
            int LenExpression(string s)
            {
                if (s.Length <= 0) return 0;
                else
                {
                    if (s[s.Length - 1] == ')')
                    {
                        s = s.Remove(s.Length - 1, 1);
                        int lenS = LenExpression(s);
                        s = s.Remove(s.Length - lenS + 1, lenS);
                        return lenS + LenExpression(s) + 1;
                    }
                    if (s[s.Length - 1] == '(') return 1;
                    else
                    {
                        s = s.Remove(s.Length - 1, 1);
                        return 1 + LenExpression(s);
                    }
                }
            }
            int Expression(string s)
            {
                int res = 0, len;
                if (s.Length > 0)
                {
                    if (s[s.Length - 1] == ')')
                    {
                        s = s.Remove(s.Length - 1, 1); len = LenExpression(s);
                        res = Expression(s); s = s.Remove(s.Length - len + 1, len);
                    }
                    if ("0123456789".Contains(s[s.Length - 1]))
                    {
                        res = Convert.ToInt16(s[s.Length] - 48);
                        s = s.Remove(s.Length - 1, 1);
                    }
                }
                if (s.Length > 0)
                {
                    if (s[s.Length - 1] == '-')
                    { s = s.Remove(s.Length - 1, 1); return Expression(s) - res; }
                    if (s[s.Length - 1] == '+')
                    { s = s.Remove(s.Length - 1, 1); return Expression(s) + res; }
                    if (s[s.Length - 1] == '*')
                    { s = s.Remove(s.Length - 1, 1); return Expression(s) * res; }
                    if (s[s.Length - 1] == '(') return res;
                }
                return res;
            }
            string s = "(7+3)"; Console.WriteLine(Expression(s));
        }
    }
    public class TDate
    {
        public TDate(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int DaysInMonth()
        {
            int res = 0;
            switch (Month)
            {
                case 2:
                    if (Year % 4 == 0 || (Year % 100 == 0 && Year % 400 != 0))
                        res = 29;
                    res = 28;
                    break;
                case 4:
                    res = 30;
                    break;
                case 1:
                    res = 31;
                    break;
                case 3:
                    goto case 1;
                case 5:
                    goto case 1;
                case 7:
                    goto case 1;
                case 8:
                    goto case 1;
                case 10:
                    goto case 1;
                case 12:
                    goto case 1;
                case 6:
                    goto case 4;
                case 9:
                    goto case 4;
                case 11:
                    goto case 4;
            }
            return res;
        }
        public int CheckDate()
        {
            if (Month < 1 && Month > 12)
                return 1;
            if (Day < DaysInMonth() && Day > 0)
                return 0;
            else
                return 2;
        }
        public override string ToString() => $"{Day}|{Month}|{Year}";
    }
}
