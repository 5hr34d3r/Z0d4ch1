﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z0d4ch1
{
    public partial class Z0d4chn1k_01
    {
        public static void Z_1_4()
        {
            string input = Console.ReadLine();
            double op;
            Console.WriteLine(double.TryParse(input, out op) ? $"{op} - You input that number" : $"{input} - Not a number");
        }
        public static void Z_1_25()
        {
            double side = double.Parse(Console.ReadLine());
            Console.WriteLine(side * 4);
        }
        public static void Z_1_42()
        {
            double e = double.Parse(Console.ReadLine());
            double f = double.Parse(Console.ReadLine());
            double g = double.Parse(Console.ReadLine());
            double h = double.Parse(Console.ReadLine());
            double a = (e + (f / 2)) / 3;
            double b = Math.Abs(Math.Pow(h, 2) - g);
            double c = Math.Sqrt(Math.Pow(g - h, 2) - 3 * Math.Sin(e));
            Console.WriteLine($"\na = {a}\nb = {b}\nc ={c}");
        }
        public static void Z_1_62()
        {
            double a = 3;
            // а
            double resa1 = a * a * a;
            double resa2 = resa1 * resa1 * resa1 * a;
            Console.WriteLine($"a in 3 = {resa1}\na in 10 = {resa2}");
            // б
            double resb1 = a * a * a * a;
            double resb2 = resa2 * resa2;
            Console.WriteLine($"a in 4 = {resb1}\na in 20 = {resb2}");
            // в
            double resv1 = a * a * a * a * a;
            double resv2 = resv1 * resv1 * resv1 * resb1;
            Console.WriteLine($"a in 5 = {resv1}\na in 13 = {resv2}");
            // г
            double resg1 = resv1;
            double resg2 = resv2 * resg1 * a;
            Console.WriteLine($"a in 5 = {resg1}\na in 19 = {resg2}");
            // д
            double resd1 = a * a;
            double resd2 = resd1 * resa1;
            double resd3 = resv2 * resb1;
            Console.WriteLine($"a in 2 = {resd1}\na in 5 = {resd2}\na in 17 = {resd3}");
            // е
            double rese1 = resb1;
            double rese2 = resa2 * resd1;
            double rese3 = resb2 * resb1 * resb1;
            Console.WriteLine($"a in 4 = {rese1}\na in 12 = {rese2}\na in 28 = {rese3}");
        }
        public static void Z_2_4()
        {
            double distance = 4253;
            Console.WriteLine($"In {distance} meters - {(int)distance / 1000} km");
        }
        public static void Z_2_13()
        {
            int value = 321;
            Console.WriteLine($"{value} in reverse is {value % 10}{value / 10 - value / 100 * 10}{value / 100}");
        }
        public static void Z_2_27()
        {
            int n = int.Parse(Console.ReadLine());
            int x = (int)((n - n % 10) / 10 + ((n % 10) * Math.Pow(10, n.ToString().Count() - 1)));
            Console.WriteLine($"original value of {n} is {x}");
        }
        public static void Z_3_4()
        {
            bool x = true;
            bool y = true;
            bool z = false;
            Console.WriteLine($"a){!x && y}");
            Console.WriteLine($"b){x || !y}");
            Console.WriteLine($"v){x || y && z}");
        }
        public static void Z_3_30()
        {
            int a = 324;
            Console.WriteLine(a % 2 == 0 || a % 3 == 0);
            Console.WriteLine(a % 3 != 0 && a % 10 == 0);
        }
        public static void Z_4_4()
        {
            double x = 2.75;
            Console.WriteLine(3 > x ? "II" : "I");
        }
        public static void Z_4_23()
        {
            int num = 16;
            Console.WriteLine(num / 10 > num % 10 ? num / 10 : num % 10);
            Console.WriteLine(num / 10 == num % 10 ? "y" : "n");
        }
        public static void Z_4_40()
        {
            double x = 8;
            if (-2.4 <= x && x <= 5.7)
                Console.WriteLine(x * x);
            else
                Console.WriteLine(4);
        }
        public static void Z_4_73()
        {
            double num1 = 3;
            double num2 = 5;
            double num3 = 8.6;
            double num4 = 9.2;
            Console.WriteLine($"a){Math.Max(Math.Max(num1, num2), Math.Max(num3, num4))}");
            Console.WriteLine($"b){Math.Min(Math.Min(num1, num2), Math.Min(num3, num4))}");
        }
        public static void Z_4_107()
        {
            //a
            int month = int.Parse(Console.ReadLine());
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                Console.WriteLine(31);
            if (month == 4 || month == 6 || month == 9 || month == 11)
                Console.WriteLine(30);
            if (month == 2)
                Console.WriteLine(28);
            //b
            month = int.Parse(Console.ReadLine());
            bool isLeapYear = bool.Parse(Console.ReadLine());
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                Console.WriteLine(31);
            if (month == 4 || month == 6 || month == 9 || month == 11)
                Console.WriteLine(30);
            if (month == 2)
            {
                if (isLeapYear)
                    Console.WriteLine(29);
                else
                    Console.WriteLine(28);
            }
        }
        public static void Z_5_4()
        {
            Console.WriteLine($"a)\t\tб)");
            for (int i = 10, j = 25; i < 26; i++, j++)
            {
                Console.Write($"{i} {i + 0.4}");
                if (j < 36)
                    Console.Write($"\t\t{j} {j + 0.5} {j - 0.2}");
                Console.WriteLine();
            }
        }
        public static void Z_5_30()
        {
            int a = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            //а
            Console.WriteLine("а)");
            for (int i = 20; i < 41; i++)
                Console.WriteLine($"{i} - {Math.Pow(i, 2)}");
            //б
            Console.WriteLine("б)");
            if (a >= 0 && a <= 50)
                for (int i = a; i <= 50; i++)
                    Console.WriteLine($"{i} - {Math.Pow(i, 2)}");
            else
                Console.WriteLine("a greater than 50 or lower then 0");
            //в
            Console.WriteLine("в)");
            if (n >= 1 && n <= 100)
                for (int i = 1; i <= n; i++)
                    Console.WriteLine($"{i} - {Math.Pow(i, 2)}");
            else
                Console.WriteLine("n greater than 100 or lower then 1");
            //г
            Console.WriteLine("г)");
            if (a <= b)
                for (int i = a; i <= b; i++)
                    Console.WriteLine($"{i} - {Math.Pow(i, 2)}");
            else
                Console.WriteLine("b greater then a");
        }
        public static void Z_5_42()
        {
            int[] marks = new int[] { 4, 5, 3, 5 };
            int avg = 0;
            foreach (var item in marks)
                avg += item;
            Console.WriteLine((double)avg / marks.Length);
        }
        public static void Z_5_69()
        {
            int n = int.Parse(Console.ReadLine());
            double[] v = new double[n];
            v[0] = 0; v[1] = 0; v[2] = 1.5;
            for (int i = 3; i < n; i++)
                v[i] = ((i - 1) / (Math.Pow(i, 2) + 1)) * v[i - 1] - v[i - 2] + v[i - 3];
            Console.WriteLine(v[n - 1]);
        }
        public static void Z_5_78()
        {
            double x1 = 1.3;
            double x2 = 2.5;
            Console.WriteLine(Math.Cos(x1) - Math.Cos(x2));
        }
        public static void Z_5_84()
        {
            double res = Math.Pow(Math.Pow(20, 2) - Math.Pow(19, 2), 2);
            for (int i = 18; i > 0; i--)
            {
                res -= Math.Pow(i, 2);
                res = Math.Pow(res, 2);
            }
            Console.WriteLine(res);
        }
        public static void Z_6_4()
        {
            int n = 5;
            double[] nums = new double[5] { -2.3, -1.2, 0.5, 2, 4 };
            int i = 0;
            while (nums[i] < 0)
            {
                Console.WriteLine(nums[i]);
                i++;
            }
        }
        public static void Z_6_25()
        {
            int[] nums = new int[] { 10, -1, -2, 58, -9, 3, 6, 0 };
            int i = 0;
            int cnt = 0;
            while (nums[i] != 0)
            {
                if (nums[i] > 0 && nums[i + 1] < 0 || nums[i] < 0 && nums[i + 1] > 0)
                    cnt++;
                i++;
            }
            Console.WriteLine($"{cnt} times the sign has changed");
        }
        public static void Z_6_49()
        {
            int n = int.Parse(Console.ReadLine());
            bool a = false, b1 = false, b2 = false;
            while (n > 1)
            {
                if (n % 10 == 3) a = true;
                if (n % 10 == 2) b1 = true;
                if (n % 10 == 5) b2 = true;
                n /= 10;
            }
            if (a) Console.WriteLine("This number include 3");
            if (b1 && b2) Console.WriteLine("This number includes 2 and 5");
        }
        public static void Z_6_83()
        {
            int n = int.Parse(Console.ReadLine()), i, a = int.Parse(Console.ReadLine()), mn = 10, mx = 0;
            while (n > 0)
            {
                i = n % 10;
                n = n / 10;
                if (mn > i) mn = i;
                if (mx < i) mx = i;
            }
            if ((mx + mn) % a == 0) Console.WriteLine("Yes");
            else Console.WriteLine("No");
        }
        public static void Z_6_91()
        {
            int num = int.Parse(Console.ReadLine()), sum = 0, mul = 1, sqSum = 0, cSum = 0, n;
            for (int i = 0; i < num.ToString().Length; i++)
            {
                n = int.Parse(num.ToString()[i].ToString());
                sum += n;
                mul *= n;
                sqSum += (int)Math.Pow(n, 2);
                cSum += (int)Math.Pow(n, 3);
            }
            Console.WriteLine($"a){num.ToString().Length}");
            Console.WriteLine($"б){sum}");
            Console.WriteLine($"в){mul}");
            Console.WriteLine($"г){(double)sum / num.ToString().Length}");
            Console.WriteLine($"д){sqSum}");
            Console.WriteLine($"е){cSum}");
            Console.WriteLine($"ж){num.ToString()[0]}");
            Console.WriteLine($"з){int.Parse(num.ToString()[0].ToString()) + int.Parse(num.ToString()[num.ToString().Length - 1].ToString())}");
        }
        public static void Z_7_4()
        {
            int a = 2, b = 9, res = 0;
            for (int i = a; i < b; i++)
            {
                if (i % 4 == 0)
                    res += i;
            }
            Console.WriteLine(res);
        }
        public static void Z_7_17()
        {
            int m = int.Parse(Console.ReadLine()), n = int.Parse(Console.ReadLine()), sum = 0;
            int[] x = new int[m];
            Console.WriteLine();
            for (int i = 0; i < x.Length; i++)
                x[i] = int.Parse(Console.ReadLine());
            Console.WriteLine("Multiplyes:");
            for (int i = 0; i < x.Length; i++)
                if (x[i] % n == 0)
                    sum += x[i];
            Console.WriteLine($"Sum of multiplyes num equal: {sum}");
        }
        public static void Z_7_34()
        {
            int m = int.Parse(Console.ReadLine()), m3c = 0, m7c = 0;
            int[] x = new int[m];
            Console.WriteLine();
            for (int i = 0; i < x.Length; i++)
                x[i] = int.Parse(Console.ReadLine());
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] % 3 == 0)
                    m3c += 1;
                if (x[i] % 7 == 0)
                    m7c += 1;
            }
            Console.WriteLine($"Count nums of multiplyes of 3 is {m3c}");
            Console.WriteLine($"Count nums of multiplyes of 7 is {m7c}");
        }
        public static void Z_7_55()
        {
            double res = -1, minTime = double.MaxValue;
            string input = "";
            while (input != "q")
            {
                input = Console.ReadLine();
                if (input != "q")
                {
                    res = double.Parse(input);
                    if (res < minTime)
                    {
                        minTime = res;
                        Console.WriteLine($"New min time for finishing is: {minTime}");
                    }
                }
            }
        }
        public static void Z_7_89()
        {
            int n = int.Parse(Console.ReadLine()), b = int.Parse(Console.ReadLine()), sum = 0;
            int[] x = new int[n];
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = int.Parse(Console.ReadLine());
                sum += x[i];
                if (sum % b == 0)
                    Console.WriteLine($"{i + 1}) {sum}");
            }

        }
        public static void Z_7_99()
        {
            int n = int.Parse(Console.ReadLine()), m = int.Parse(Console.ReadLine()), q = int.Parse(Console.ReadLine()), sum = 0;
            int[] a = new int[n];
            Console.WriteLine();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = int.Parse(Console.ReadLine());
                sum += a[i];
                if (sum < m && sum > q)
                    Console.WriteLine($"yes");
                else
                    Console.WriteLine("no");
            }
        }
        public static void Z_8_4()
        {
            //а
            Console.WriteLine("a)");
            for (int i = 0; i < 5; i++)
            {
                for (int j = i; j > -1; j--)
                    Console.Write($"{j} ");
                Console.WriteLine();
            }
            //б
            Console.WriteLine("\nб)");
            for (int i = 6; i > 1; i--)
            {
                for (int j = i; j > 1; j--)
                    Console.Write($"{j} ");
                Console.WriteLine();
            }
            //в
            Console.WriteLine("\nв)"); int strt = 0;
            for (int i = 30; i > 25; i--)
            {
                if (strt == 0)
                    strt = i;
                for (int j = strt; j < 31; j++)
                    Console.Write($"{j} ");
                strt = i - 1;
                Console.WriteLine();
            }
            //г
            Console.WriteLine("\nг)"); strt = 5;
            for (int i = 20; i > 15; i--)
            {
                for (int j = i; j < i + strt; j++)
                    Console.Write($"{j} ");
                strt--;
                Console.WriteLine();
            }
        }
        public static void Z_8_28()
        {
            int k = 0;
            for (int i = 200; i <= 500; i++)
            {
                k = 0;
                for (int j = 1; j <= i; j++)
                {
                    if (i % j == 0)
                        k += 1;
                    if (i == j && k == 6)
                        Console.WriteLine($"{i} ");
                }
            }
        }
        public static void Z_8_45()
        {
            string str = "";
            for (int i = 100; i <= 999; i++)
            {
                str = i.ToString();
                if (str[0] != str[1] && str[1] != str[2] && str[0] != str[2])
                    Console.WriteLine(str);
            }
        }
        public static void Z_9_4()
        {
            string capital = Console.ReadLine(), state = Console.ReadLine();
            Console.WriteLine($"Столица государства {state} - город {capital}");
        }
        public static void Z_9_16()
        {
            string str = Console.ReadLine();
            Console.WriteLine(str[1] == str[3] ? "yes" : "no");
        }
        public static void Z_9_44()
        {
            string s = Console.ReadLine();
            Console.WriteLine(new string(s.Reverse().ToArray()));
        }
        public static void Z_9_78()
        {
            string s = Console.ReadLine();
            Console.WriteLine(s == new string(s.Reverse().ToArray()) ? "yes" : "no");
        }
        public static void Z_9_93()
        {
            string inp = Console.ReadLine();
            for (int i = 0; i < inp.Length; i++)
                if ((i + 1) % 3 == 0)
                    inp = inp.Remove(i, 1).Insert(i, "A");
            Console.WriteLine(inp);
        }
        public static void Z_9_110()
        {
            bool oR = false, lR = false;
            string str = Console.ReadLine(), tmp = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (!oR && str[i] == 'о')
                {
                    str = str.Remove(i, 1);
                    oR = true;
                }
                tmp = new string(str.Reverse().ToArray());
                if (!lR && tmp[i] == 'л')
                {
                    tmp = tmp.Remove(i, 1);
                    lR = true;
                    str = new string(tmp.Reverse().ToArray());
                }
            }
            Console.WriteLine(str);
        }
        public static void Z_9_141()
        {
            string str = "ds1aa4d4a8s64da"; int tmpn = 0, sum = 0, max = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (int.TryParse(str[i].ToString(), out tmpn))
                {
                    sum += tmpn;
                    max = max < tmpn ? tmpn : max;
                }
            }
            Console.WriteLine($"а){sum}\nб){max}");
        }
        public static void Z_9_155()
        {
            string str = "fjddhay"; bool sc = false;
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = i + 1 < str.Length ? i + 1 : i; j < str.Length; j++)
                {
                    if (str[i] == str[j])
                    {
                        sc = true;
                        Console.WriteLine($"{str[i]} in positions {i + 1} and {j + 1}");
                        break;
                    }
                }
                if (sc)
                    break;
            }
        }
        public static void Z_9_172()
        {
            string sentence = "dasda dasdadaw fdsfg fsd";
            string[] parts = sentence.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine(parts.OrderByDescending(p => p.Length).FirstOrDefault());
        }
        public static void Z_10_4()
        {
            //1
            int a = 4, b = 2;
            Console.WriteLine("1)" + (a > 3 * b ? a : 3 * b) * (2 * a - b > 2 * b ? 2 * a - b : 2 * b));
            //2
            static int Min(int n1, int n2) => n1 > n2 ? n1 : n2;
            Console.WriteLine("2)" + Min(a, 3 * b) * Min(2 * a - b, 2 * b));
        }
        public static void Z_10_36()
        {
            int cnt = int.Parse(Console.ReadLine());
            Draw(cnt);
            static void Draw(int count)
            {
                Console.Clear();
                for (int i = 0; i < count; i++)
                    Console.WriteLine("*");
            }
        }
        public static void Z_10_44()
        {
            int num = 232;
            Console.WriteLine(NumericSquare(num));
            static int NumericSquare(int n)
            {
                if (n == 0) return 0;
                else return n % 10 + NumericSquare(n / 10);
                if (n > 9) return NumericSquare(n);
            }
        }
        public static void Z_11_4()
        {
            int[] heights = new int[12];
            Random rnd = new Random();
            for (int i = 0; i < heights.Length; i++)
            {
                heights[i] = rnd.Next(163, 190);
                Console.WriteLine(heights[i]);
            }
        }
    }
}
